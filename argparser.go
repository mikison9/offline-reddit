package main

import (
	"flag"
	"fmt"
	"os"
)

var argImgOnly bool
var argPostCount int
var argSort string

func parseArgs(osArgs []string) string {

	if len(osArgs) < 2 {
		fmt.Println("Usage: offline-reddit redditname")
		os.Exit(69)
	}

	argSort = "hot"

	f := flag.NewFlagSet("offline-reddit", flag.ExitOnError)

	count := f.Int("count", 100, "Number of posts to download")
	bynew := f.Bool("by-new", false, "Specify to sort by new")
	imageonly := f.Bool("image-only", false, "Specify to only download images and dont posts into folders")

	if osArgs[1] == "-h" || osArgs[1] == "--help" {
		f.Parse(osArgs[1:])
		return ""
	}

	f.Parse(osArgs[2:])

	argPostCount = *count

	if *bynew {
		argSort = "new"
	}

	argImgOnly = *imageonly

	/*fmt.Printf("count: %d\n", *count)
	fmt.Printf("bynew: %t\n", *bynew)
	fmt.Printf("imageonly: %t\n", *imageonly)*/

	return osArgs[1]
}
