package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"
	"strings"
	"time"

	"github.com/buger/jsonparser"
)

type jsonParseError struct {
	S string
}

func (e *jsonParseError) Error() string {
	return e.S
}

func jsonGet(data []byte, key string) ([]byte, error) {
	newData, _, _, err := jsonparser.Get(data, key)
	if err != nil {
		return nil, &jsonParseError{
			S: "Error at key: " + key + ", " + err.Error(),
		}
	}

	return newData, nil
}

func jsonGetBool(data []byte, key string) (bool, error) {
	boolos, err := jsonparser.GetBoolean(data, key)
	if err != nil {
		return false, &jsonParseError{
			S: "Error at key: " + key + ", " + err.Error(),
		}
	}
	return boolos, nil
}

func jsonGetInt(data []byte, key string) (int64, error) {
	intos, err := jsonparser.GetInt(data, key)
	if err != nil {
		return 0, &jsonParseError{
			S: "Error at key: " + key + ", " + err.Error(),
		}
	}
	return intos, nil
}

func downloadSubreddit(fullName string) {
	fullName = sanitizeSubredditName(fullName)
	if !detectytdlRan {
		detectytdl()
	}

	subName := getRedditName(fullName)

	postPages := getSubredditPosts(fullName)

	os.Mkdir(subName, os.ModePerm)

	//fmt.Printf("%s\n", posts)

	for i, page := range postPages {
		parsePage(page.Posts, page.Count, subName, i*100)
	}
}

func parsePage(posts []byte, postCount int, subName string, offset int) {

	for i := 0; i < int(postCount+1); i++ {
		toGet := fmt.Sprintf("[%d]", i)
		postData, err := jsonGet(posts, toGet)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		processPost(postData, i+offset, subName)
	}

	downloadWaitGroup.Wait()
}

func parsePost(data []byte) (title []byte, selftext []byte, domain []byte, url string, isVideo bool) {

	title, err := jsonGet(data, "title")
	if err != nil {
		fmt.Println(err.Error())
	}

	tmpselftext, err := jsonGet(data, "selftext")
	if err != nil {
		fmt.Println(err.Error())
	} else {
		selftext = make([]byte, len(tmpselftext))
		copy(selftext, tmpselftext)
		selftext = append(selftext[:], '\n')
	}

	tmpURL, err := jsonGet(data, "url")
	if err != nil {
		fmt.Println(err.Error())
	} else {
		url = fmt.Sprintf("%s", tmpURL)
	}

	domain, err = jsonGet(data, "domain")
	if err != nil {
		fmt.Println(err.Error())
	}

	isVideo, err = jsonGetBool(data, "is_video")
	if err != nil {
		fmt.Println(err.Error())
	}

	return
}

func processPost(json []byte, pos int, redditName string) int {
	pos++

	//fmt.Printf("processing post num: %d\n", pos)

	poStr := fmt.Sprintf("%d", pos)

	data, err := jsonGet(json, "data")
	if err != nil {
		fmt.Println(err.Error())
		return 69
	}

	title, selftext, domain, url, isVideo := parsePost(data)

	if bytes.Equal([]byte("youtube.com"), domain) || bytes.Equal([]byte("youtu.be"), domain) {
		isVideo = true
	}

	if argImgOnly {
		os.Mkdir(redditName, os.ModePerm)
		goto download
	}

	os.Mkdir(redditName+string(os.PathSeparator)+poStr, os.ModePerm)
	//fmt.Printf("%s\n", title)
	//fmt.Printf("url: %s\n", url)
	//fmt.Printf("is_video: %d\n", isVideo)

	if title != nil {
		err = ioutil.WriteFile(redditName+string(os.PathSeparator)+poStr+string(os.PathSeparator)+fmt.Sprintf("%s", title)+".txt", selftext, os.ModePerm)
	}

download:
	for {
		if runtime.NumGoroutine() > 50 {
			time.Sleep(time.Millisecond * 50)
		} else {
			downloadWaitGroup.Add(1)
			go handleDownload(url, isVideo, poStr, redditName)
			break
		}
	}

	return 0
}

func sanitizeSubredditName(name string) string {
	nameout := name

	if name[0] != '/' {
		nameout = "/" + nameout
	}
	if name[len(name)-1] == '/' {
		nameout = nameout[:len(nameout)-1]
	}

	return nameout
}

func getRedditName(subreddit string) string {
	return subreddit[strings.LastIndex(subreddit, "/")+1:]
}
