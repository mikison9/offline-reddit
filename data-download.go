package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"sync"
	"unicode"
)

var ytdlInstalled bool
var detectytdlRan bool

var downloadWaitGroup sync.WaitGroup

func handleDownload(url string, isVideo bool, postNum string, subname string) {
	defer downloadWaitGroup.Done()

	if filename := getFileName(url); filename != "" && !isVideo && url != "" {
		data, err := getData(url)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			filename := getFileName(url)
			//fmt.Printf("filename: %s\n", filename)
			if argImgOnly {
				saveFile(subname+string(os.PathSeparator)+filename, data)
			} else {
				saveFile(subname+string(os.PathSeparator)+postNum+string(os.PathSeparator)+filename, data)
			}
		}
	} else if ytdlInstalled && !argImgOnly {
		downloadVideo(url, postNum, subname)
	}
}

func getData(url string) (io.Reader, error) {

	/*fmt.Printf("downloading: %s\n", url)
	defer fmt.Printf("downloaded: %s\n", url)*/

	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return nil, err
	}

	req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0")

	res, err := (&http.Client{}).Do(req)

	if err != nil {
		return nil, err
	}

	return res.Body, nil
}

func downloadVideo(url string, postNum string, subname string) {
	runCount := 0

	//fmt.Printf("running command: youtube-dl %s\n", url)

	for {
		cmd := exec.Command("youtube-dl", url, "-o", "video.mp4", "-f", "mp4")
		wd, err := os.Getwd()
		if err != nil {
			return
		}

		if argImgOnly {
			cmd.Dir = wd + string(os.PathSeparator) + subname
		} else {
			cmd.Dir = wd + string(os.PathSeparator) + subname + string(os.PathSeparator) + postNum
		}

		_, err = cmd.Output()
		runCount++
		if err != nil {
			if runCount == 2 {
				fmt.Printf("Error downloading video: %s, %s\n", url, err.Error())
				os.Remove(wd + string(os.PathSeparator) + postNum + "video" + ".mp4")
				break
			}
			os.Remove(wd + string(os.PathSeparator) + postNum + "video" + ".mp4")
		} else {
			break
		}
	}

	//fmt.Printf("youtube-dl output: %s\n", output)
}

func detectytdl() {
	defer func() {
		detectytdlRan = true
	}()

	cmd := exec.Command("youtube-dl", "--version")

	output, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		fmt.Println("Not downloading videos")
		ytdlInstalled = false
		return
	}

	if unicode.IsDigit(rune(output[0])) && unicode.IsDigit(rune(output[1])) {
		ytdlInstalled = true
	} else {
		ytdlInstalled = false
	}
}

func getFileName(url string) string {
	if strings.Contains(url, "?") {
		return url[strings.LastIndex(url, "/")+1 : strings.LastIndex(url, "?")]
	}
	return url[strings.LastIndex(url, "/")+1:]
}

func saveFile(path string, data io.Reader) {
	file, err := os.Create(path)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer file.Close()

	_, err = io.Copy(file, data)
	if err != nil {
		fmt.Println(err.Error())
	}
}
