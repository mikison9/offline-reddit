package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"strconv"
)

var apiHttpClient *http.Client

type postPage struct {
	Posts []byte
	Count int
}

func parsePostsFromRes(resp *http.Response) (posts []byte, postCount int64, after []byte, parseError error) {
	jsonData, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, 0, nil, err
	}

	data, err := jsonGet(jsonData, "data")

	if err != nil {
		errornum, err := jsonGet(jsonData, "error")
		if err == nil {
			errormsg, err := jsonGet(jsonData, "message")
			if err == nil {
				fmt.Printf("Api response:\n%s: %s\n", errornum, errormsg)
				os.Exit(69)
			}
		}

		return nil, 0, nil, err
	}

	postCount, err = jsonGetInt(data, "dist")

	if err != nil {
		return nil, 0, nil, err
	}

	postCount--

	posts, err = jsonGet(data, "children")

	if err != nil {
		return nil, 0, nil, err
	}

	after, err = jsonGet(data, "after")

	if err != nil {
		return nil, 0, nil, err
	}

	return posts, postCount, after, nil
}

func createRequest(subredditName string, limit int, after []byte) *http.Request {
	var req *http.Request
	var err error

	if after != nil {
		req, err = http.NewRequest("GET", "https://www.reddit.com"+subredditName+"/"+argSort+".json"+"?limit="+strconv.Itoa(limit)+"&after="+string(after), nil)
	} else {
		req, err = http.NewRequest("GET", "https://www.reddit.com"+subredditName+"/"+argSort+".json"+"?limit="+strconv.Itoa(limit), nil)
	}

	//fmt.Printf("created request to: %s\n", req.URL.String())

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(69)
	}

	req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0")

	return req
}

func getSubredditPosts(name string) []postPage {
	if apiHttpClient == nil {
		apiHttpClient = &http.Client{}
	}

	var req *http.Request
	var postPages []postPage

	if argPostCount <= 100 {
		req = createRequest(name, argPostCount, nil)
		res, err := apiHttpClient.Do(req)

		if err != nil {
			fmt.Println(err.Error())
			os.Exit(69)
		}

		posts, postCount, _, err := parsePostsFromRes(res)

		if err != nil {
			fmt.Println(err.Error())
			os.Exit(69)
		}

		postPages := make([]postPage, 1)

		postPages[0] = postPage{
			Posts: posts,
			Count: int(postCount),
		}

		return postPages

	} else {
		remainingPostCount := argPostCount
		pageCount := int(math.Ceil(float64(argPostCount) / 100.0))
		postPages = make([]postPage, pageCount)

		//fmt.Printf("page count: %d\n", pageCount)

		var after []byte

		for i := 0; i < pageCount; i++ {
			if remainingPostCount > 100 {
				req = createRequest(name, 100, after)

			} else {
				req = createRequest(name, remainingPostCount, after)
			}

			res, err := apiHttpClient.Do(req)

			if err != nil {
				fmt.Println(err.Error())
				os.Exit(69)
			}

			posts, postCount, tmpAfter, err := parsePostsFromRes(res)
			after = tmpAfter

			if err != nil {
				fmt.Println(err.Error())
				os.Exit(69)
			}

			postPages[i] = postPage{
				Posts: posts,
				Count: int(postCount),
			}

			remainingPostCount = remainingPostCount - 100

			if remainingPostCount < 0 {
				break
			}
		}
		return postPages
	}

	return nil
}
