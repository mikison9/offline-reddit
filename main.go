package main

import (
	"os"
)

func main() {
	subname := parseArgs(os.Args)
	if subname != "" {
		downloadSubreddit(subname)
	}
}
